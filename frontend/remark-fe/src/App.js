import Menu from "./components/menu";
import {Container} from "semantic-ui-react";
import {Outlet} from "react-router-dom";
import RunIdContextProvider from "./components/context/runIdContext";
import ContextWrapper from "./components/context/ContextWrapper";
import {Fragment} from "react";

function App() {

    return (
        <Fragment>
            <ContextWrapper>
                {children =>
                    <div id="main" style={{minWidth: "600px"}}>
                        <Menu />
                        <Container>
                            <div id="detail">
                                <Outlet />
                            </div>
                        </Container>
                    </div>
                }
            </ContextWrapper>
        </Fragment>

    );
}

export default App;
