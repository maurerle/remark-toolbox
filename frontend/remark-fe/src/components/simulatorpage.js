import {Button, Divider, Dropdown, Icon} from "semantic-ui-react";
import {useContext, useEffect, useState} from "react";
import GetScenarios from "../API/getScenarios";
import PostScenario from "../API/postScenario";
import GetRun from "../API/getRun";
import ExampleYaml from "../exampleYaml";
import ScenarioConstructor from "./simulatorComponents/scenarioConstruktor";
import RunIdContext from "./context/runIdContext"

function SimulatorPage() {

    const [scenario, setScenario] = useState(ExampleYaml())
    const [scenariosAreLoaded, setScenariosAreLoaded] = useState(false);
    const [scenarios, setScenarios] = useState(undefined);
    const [dropDownScenario, setDropDownScenario] = useState("Choose Scenario");

    const [uploadScenarioId, setUploadScenarioId] = useState(undefined)
    const [runScenarioId, setRunScenarioId] = useState(undefined)
    //todo lift runid up to create a new analytics page
    const {runId, setRunId} = useContext(RunIdContext)
    const [uploadingScenario, setUploadingScenario] = useState(false)
    const [startingSimulation, setStartingSimulation] = useState(false)


    //todo the scenarios should be dynamicly loaded from the server when the page is opened
    const scenarioOptions = [
        {key: 'default', text: 'Default Scenario', value: 'Default Scenario'},
        {key: 'Scenario1', text: 'Scenario 1', value: 'Scenario 1'},
        {key: 'Scenario2', text: 'Scenario 2', value: 'Scenario 2'},
    ]

    function changeScenarioYaml(scenario) {
        setScenario(scenario)
    }

    useEffect(() => {
        GetScenarios()
            .then(
                (result) => {
                    //todo destruct scenarios to construct right format for scenariosOptions
                    console.log(result)
                    setScenarios(scenarioOptions)
                    setScenariosAreLoaded(true)
                }
            )
    }, [])



    function onChangeDropDown(e){
        setDropDownScenario(e.target.value)
        //todo get scenarioyaml via api call

    }

    function uploadScenario() {
        setUploadingScenario(true)
        //todo validate name and the scenario.yaml
        console.log(scenario)
        console.log("scenario")
        PostScenario(scenario)
            .then((result) => {
                setUploadScenarioId(result["scenario_id"])
            })
        setUploadingScenario(false)
    }

    useEffect(() => {
        //todo GetScenarios should be called here
    }, [uploadScenarioId])

    function startSimulation() {
        setStartingSimulation(true)
        //todo validate name and the scenario.yaml


        PostScenario(ExampleYaml())
            .then((result) => {
                setRunScenarioId(result["scenario_id"])
            })
    }

    useEffect(() => {
        if(runScenarioId != undefined) {
            GetRun(runScenarioId)
                .then((result) => {
                    setRunId(result["run_id"])
                })
            setStartingSimulation(false)
        }
    }, [runScenarioId])

    useEffect(()=>{

        console.log("runid in effect: " + runId)

    },[runId])

    return (
        <div>
            <div className="ui segment">
                <h1>Configure Scenario</h1>
                <div className="ui blue segment">
                    <div className="ui stackable two column grid">
                        <div className="column" id="content-column">
                            <h4>Simulation Name:</h4>
                            <div className="ui input">
                                <input type="text" placeholder="Choose name..."/>
                            </div>
                        </div>
                        <div className="column" id="content-column">
                            <h4>Preconfigured Scenarios:</h4>
                            <Dropdown
                                button
                                className='icon'
                                floating
                                labeled
                                onChange={onChangeDropDown}
                                icon='file alternate'
                                options={scenarios}
                                search
                                text={dropDownScenario}
                            />
                            {
                                scenariosAreLoaded ? (
                                    <></>
                                ) : (
                                    <Icon
                                        name="spinner"
                                        loading={true}
                                    />
                                )}
                        </div>
                    </div>
                </div>
                <Divider />
                <ScenarioConstructor
                    scenarioYaml = {scenario}
                    changeScenarioYaml = {changeScenarioYaml}
                />
            </div>
            <div className="ui stackable two column grid">
                <div className="column" id="content-column">
                    <Button color="linkedin" style={{marginBottom: "1em"}} onClick={startSimulation}>Start
                        Simulation</Button>
                    {
                        startingSimulation ? (
                            <Icon
                                name="spinner"
                                loading={true}
                            />
                        ) : (
                            <></>
                        )
                    }
                </div>
                <div className="column" id="content-column" hidden={true}>
                    <Button color="linkedin" style={{marginBottom: "1em"}} onClick={uploadScenario}>Save
                        Scenario</Button>
                    {
                        uploadingScenario ? (
                            <Icon
                                name="spinner"
                                loading={true}
                            />
                        ) : (
                            <></>
                        )
                    }
                </div>
            </div>
        </div>

    )

}

export default SimulatorPage
