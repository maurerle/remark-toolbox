import React, {useEffect, useState} from "react";
import RunIdContext from "./runIdContext";

function ContextWrapper({children}) {
    const [runId, setRunId] = useState("")
    const runIdValue = {runId, setRunId}


  return (
      <RunIdContext.Provider value={runIdValue}>
          <>{children()}</>
      </RunIdContext.Provider>

  )
}

export default ContextWrapper