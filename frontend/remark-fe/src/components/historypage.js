const HistoryPage = () => {

    return (
        <div>
            <div className="ui segment">
                <h3>History:</h3>
                <table className="ui celled table">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Started</th>
                        <th>Run Time</th>
                        <th>Finished</th>
                        <th>Successfully</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td data-label="Name">Merit Order Model</td>
                        <td data-label="Started">12.12.22 13:12</td>
                        <td data-label="Run Time">~4 days</td>
                        <td data-label="Finished">16.12.22 14:19 days</td>
                        <td data-label="Successfully">Error</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    )
}

export default HistoryPage