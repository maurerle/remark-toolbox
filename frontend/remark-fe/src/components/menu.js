import {Icon} from 'semantic-ui-react'
import {Link} from "react-router-dom";

function Menu() {

    return (
            <div className="ui left fixed vertical menu">
                <Link to="home" className="item">
                    <img src={require("../resources/images/logo192.png")} alt="Logo" height="100px" width="100px"
                         style={{marginLeft: "30px"}}/>
                </Link>
                <Link to="simulator" className="item">
                    <Icon name="sliders horizontal"/> Simulator
                </Link>
                <Link to="analytics" className="item">
                    <Icon name="chart bar"/> Analytics
                </Link>
                <Link to="history" className="item">
                    <Icon name="archive"/>History
                </Link>
                <Link to="configuration" className="ui dropdown item">
                    <Icon name="cog"/> Configurations
                </Link>
            </div>
    )
}

export default Menu
