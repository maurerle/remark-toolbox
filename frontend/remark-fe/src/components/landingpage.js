import {Divider} from "semantic-ui-react";

const LandingPage = () => {

    return (
        <div>
            <div className="ui segment">
                <h1>REMARK Toolbox - Resilience in the digital power grid</h1>
                <Divider/>
                <div className="ui stackable two column grid">
                    <div className="column" id="content-column">
                        <div className="ui blue segment">
                            <h3>On going simulations:</h3>
                            <table className="ui tablet stackable celled table">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Started</th>
                                    <th>Run Time</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td data-label="Name">Merit Order Model</td>
                                    <td data-label="Started">12.12.22 13:12</td>
                                    <td data-label="Run Time">~4 days</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div className="column" id="content-column">
                        <div className="ui green segment">
                            <h3>Terminated simulations:</h3>
                            <table className="ui tablet stackable celled table">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Started</th>
                                    <th>Run Time</th>
                                    <th>Finished</th>
                                    <th>Successfully</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td data-label="Name">Merit Order Model</td>
                                    <td data-label="Started">12.12.22 13:12</td>
                                    <td data-label="Run Time">~4 days</td>
                                    <td data-label="Finished">16.12.22 14:19 days</td>
                                    <td data-label="Successfully">Error</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}


export default LandingPage
