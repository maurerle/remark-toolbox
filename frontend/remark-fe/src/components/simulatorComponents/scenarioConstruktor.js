import PowerGridConfigurator from "./powerGridConfigurator";
import {Divider} from "semantic-ui-react";
import AgentConfigurator from "./agentConfigurator";
import MarketConfigurator from "./marketConfigurator";
import {useEffect, useState} from "react";
import YAML from "yaml"

/***
 * This function should handle the deconstruction of a scenario and the reconstruction after a scenario is alterd
 * @returns {JSX.Element}
 */
function ScenarioConstructor({scenarioYaml, changeScenarioYaml}) {

    const [scenario, setScenario] = useState(YAML.parse(scenarioYaml))
    const [scenarioYAML, setScenarioYAML] = useState(scenarioYaml)
    const [powerGridCheck, setPowerGridCheck] = useState(false)
    const [agentCheck, setAgentCheck] = useState(false)
    const [marketCheck, setMarketCheck] = useState(false)

    useEffect(() => {
        changeScenarioYaml(YAML.stringify(scenarioYAML))
    }, [scenario])

    function changeScenario(scenario){
        setScenario(scenario)
    }

    const togglePowerGridCheck = () => {
        setPowerGridCheck(!powerGridCheck)
    }
    const toggleAgentCheck = () => {
        setAgentCheck(!agentCheck)
    }
    const toggleMarketCheck = () => {
        setMarketCheck(!marketCheck)
    }

    return (
        <div>
            <div className="ui orange segment">
                <h3>Power Grid Configuration</h3>
                <div className="ui fitted toggle checkbox" id="toggle-h3">
                    <input type="checkbox" onChange={togglePowerGridCheck}/>
                    <label></label>
                </div>
                <div hidden={!powerGridCheck}>
                    <PowerGridConfigurator
                        bScenario = {scenario}
                        changeScenario = {changeScenario}
                    />
                </div>
            </div>
            <Divider/>
            <div className="ui teal segment">
                <h3>Agent Configuration</h3>
                <div className="ui fitted toggle checkbox" id="toggle-h3">
                    <input type="checkbox" onChange={toggleAgentCheck}/>
                    <label></label>
                </div>
                <div hidden={!agentCheck}>
                    <AgentConfigurator
                        bScenario = {scenario}
                        changeScenario = {changeScenario}
                    />
                </div>
            </div>
            <Divider/>
            <div className="ui green segment">
                <h3>Market Configuration</h3>
                <div className="ui fitted toggle checkbox" id="toggle-h3">
                    <input type="checkbox" onChange={toggleMarketCheck}/>
                    <label></label>
                </div>
                <div hidden={!marketCheck}>
                    <MarketConfigurator
                        bScenario = {scenario}
                        changeScenario = {changeScenario}
                    />
                </div>
            </div>
        </div>
    )
}

export default ScenarioConstructor