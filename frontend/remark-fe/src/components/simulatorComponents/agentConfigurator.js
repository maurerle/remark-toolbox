import {useEffect, useState} from "react";

function AgentConfigurator({bScenario, changeScenario}) {
    const [scenario, setScenario] = useState(bScenario)
    const [loadAgents, setLoadAgents] = useState(undefined)
    const [batteryAgents, setBatteryAgents] = useState(undefined)
    const [pvAgents, setPvAgents] = useState(undefined)
    const [traders, setTraders] = useState(undefined)

    useEffect(() => {
        setLoadAgents(scenario.remark["remark_agents_params"]["remark_scope"]["load_agents"])
        setBatteryAgents(scenario.remark["remark_agents_params"]["remark_scope"]["battery_agents"])
        setPvAgents(scenario.remark["remark_agents_params"]["remark_scope"]["pv_agents"])
        setTraders(scenario.remark["remark_agents_params"]["remark_scope"]["traders"])
    }, [scenario])



    return (
        <div>
            <h3>Load Agents:</h3>
            <h3>Battery Agents:</h3>
            <h3>PV Agents:</h3>
            <h3>Trader:</h3>

        </div>
    )

}
export default AgentConfigurator