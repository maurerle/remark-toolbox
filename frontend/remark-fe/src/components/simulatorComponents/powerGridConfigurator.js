import {useEffect, useState} from "react";
import {Slider} from "react-semantic-ui-range"
import {Button} from "semantic-ui-react";

function PowerGridConfigurator({bScenario, changeScenario}) {

    const [scenario, setScenario] = useState(bScenario)
    const [grid, setGrid] = useState(undefined)
    const [start, setStart] = useState(undefined)
    const [end, setEnd] = useState(undefined)
    const [weather, setWeather] = useState(undefined)
    const [derMapping, setDerMapping] = useState(undefined)
    const [derWeatherProvider, setDerWeatherProvider] = useState(undefined)
    const [sliderValue, setSliderValue] = useState(1)


    useEffect(() => {
        if (scenario !== undefined && scenario !== null) {
            console.log(scenario)
            setGrid(scenario.remark["powergrid_params"]["remark_scope"]["gridfile"])
            setStart(scenario.remark["start_date"])
            setEnd(scenario.remark["end"])
            setWeather(scenario.remark["weather_params"]["my_weather_station"]["weather_mapping"]["WeatherCurrent"][0]["interpolate"])
            setDerMapping(scenario.remark["der_params"]["remark_scope"]["mapping"])
            setDerWeatherProvider(scenario.remark["der_params"]["remark_scope"]["weather_provider_mapping"]["BAT"])
        }
    }, [scenario])


    const sliderSettings = {
        start: 1,
        min: 1,
        max: 365,
        step: 1,
        onChange: value => {
            setSliderValue(value)
        }
    }

    function alterScenario(){

        let endValue = (sliderValue * 24) + "*60*60"
        let newScenario = scenario
        newScenario.remark["end"] = endValue
        setScenario(newScenario)

        changeScenario(scenario)
    }

    return (
        <div>
            <h3></h3>
            <p>Powergrid: {grid}</p>
            <p>Start Time: {start}</p>

            <p>End Time: {sliderValue} Days</p>
            <Slider value={sliderValue} color="orange" settings={sliderSettings}/>

            <h3>Weather: </h3>
            <p>Interpolate: {weather}</p>

            <h3>Distributed Energy Resources: </h3>
            <p>Weather Provider: {derWeatherProvider} </p>
            <Button color="linkedin" style={{marginBottom: "1em"}} onClick={alterScenario}>Apply</Button>

        </div>

    )
}

export default PowerGridConfigurator