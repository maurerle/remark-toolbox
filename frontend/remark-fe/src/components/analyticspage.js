import {useContext, useEffect, useState} from "react";
import RunIdContext from "./context/runIdContext";

const AnalyticsPage = () => {

    const {runId, setRunId} = useContext(RunIdContext)
    const [grafanaLInk, setGrafanaLInk] = useState("")


    useEffect(()=> {
        console.log("runId in Analytics: " + runId)
        setGrafanaLInk("http://localhost:3000/d/MVpw8Wv4z" +
            "/remark?orgId=1&refresh=5s&var-exp_id=" +
            runId +
            "&from=1483225200000&to=1483311600000")

    }, [runId])

    return (
        <div>
            <div className="ui segment">
                <div>Implement a search function</div>
                <div>
                    Implement tab, where a dashboard automatically opens, if a sim ist started via getRun.
                    <h3>Dashboard:</h3>
                    <iframe src={grafanaLInk} name="Simulation Dashboard" width="800" height="600"/>
                    some embedded iFrame from grafana
                    example frame link:
                    http://localhost:3000/d/MVpw8Wv4z/remark?orgId=1&refresh=5s&var-exp_id=run_245190d8-d6e4-4e6a-87c2-1f54ede60d09&from=1483225200000&to=1483311600000
                </div>
            </div>
        </div>
    )
}


export default AnalyticsPage
