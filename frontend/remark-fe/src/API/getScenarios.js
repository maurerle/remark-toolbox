import YAML from "yaml";

async function GetScenarios() {
    let errorMsg = null
    let res

    await fetch("http://localhost:5000/scenarios", {mode: 'cors', method: "GET"})
        .then(res => res.text())
        .then(
            (result) => {
                res = YAML.parse(result)
            },
            (error) => {
                //todo embed error component to display a generic errorMsg via modal
                errorMsg = error;
            }
        )

    if (errorMsg !== null) {
        console.log(errorMsg)
    }
    return res
}

export default GetScenarios
