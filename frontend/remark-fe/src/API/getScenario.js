import YAML from "yaml";


async function GetScenario(scenarioId) {

    let errorMsg = null
    let res

    await fetch("http://localhost:5000/scenarios/" + scenarioId.toString(),
        {
            mode: 'cors',
            method: "GET",
        })
        .then(res => res.text())
        .then(
            (result) => {
                res = YAML.parse(result)
                console.log(res)
            },
            (error) => {
                errorMsg = error
            }
        )


    if (errorMsg !== null) {
        console.log("error: " + errorMsg)
    }
    return res
}

export default GetScenario