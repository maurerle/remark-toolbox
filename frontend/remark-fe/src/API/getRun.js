import YAML from "yaml";

/**
 * Also start the simulation with the given scenario-id
 * @param scenarioId
 * @returns {Promise<*>}  A promise with the run-id or the error is given back.
 */
async function GetRun(scenarioId) {

    let errorMsg = null
    let res

    await fetch("http://localhost:5000/runs/" + scenarioId.toString(),
        {
            mode: 'cors',
            method: "GET",
        })
        .then(res => res.text())
        .then(
            (result) => {
                res = YAML.parse(result)
                console.log(res)
            },
            (error) => {
                errorMsg = error
            }
        )


    if (errorMsg !== null) {
        console.log("error: " + errorMsg)
    }
    return res
}

export default GetRun