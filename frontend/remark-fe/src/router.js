import {createBrowserRouter} from "react-router-dom";
import ErrorPage from "./components/errorpage";
import SimulatorPage from "./components/simulatorpage";
import AnalyticsPage from "./components/analyticspage";
import HistoryPage from "./components/historypage";
import ConfigurationPage from "./components/configurationpage";
import LandingPage from "./components/landingpage";
import React from "react";
import App from "./App";

function Router() {
    const router = createBrowserRouter([
        {
            path: "/",
            element: <App />,
            errorElement: <ErrorPage />,
            children: [
                {
                    path: "",
                    element: <LandingPage/>
                },
                {
                    path: "home",
                    element: <LandingPage />
                },
                {
                    path: "Simulator",
                    element:<SimulatorPage />,
                },
                {
                    path: "analytics",
                    element: <AnalyticsPage />
                },
                {
                    path: "history",
                    element: <HistoryPage />
                },
                {
                    path: "configuration",
                    element: <ConfigurationPage />
                },
            ]
        },
    ]);

    return router
}

export default Router()