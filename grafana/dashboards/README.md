Creating Persistent Dashboards
==============================

Persistent dashboards can be created by adding .json files to this folder. To create
such a file, open Grafana and create the dashboard there. After saving it, two
symbols will appear next to the dashboard’s title in the top left: a star to mark
it as a favorite and a ‘share’ symbol. Click on the share symbol then ‘Export’.
Do not turn on “Export for sharing externally”. Either click “Save to file” and place
the file in this folder or click “View JSON” and copy the JSON to a new file in this
folder. The dashboard should appear in Grafana after a moment.

To make the dashboard configurable, you need to add variables. To do that, go to the
“Dashboard setting” (cog symbol in the top right), then “Variables” and “Add variable”.
To create a variable that allows picking the experiment ID (i.e. the measurement in
Influx), change the “Data source” to “InfluxDB” and pick the “Sample Query” named
“Schema Exploration: (measurements)”. Change the name to `exp_id`. Click “Apply”.
Do not forget to save your dashboard as well.

In Flux queries in the rest of the dashboard, you can use `${exp_id}` to refer to the
selected experiment run. Often, your queries will begin with

```flux
from(bucket: "remark")
  |> range(start: v.timeRangeStart, stop:v.timeRangeStop)
  |> filter(fn: (r) => r._measurement == "${exp_id}")
```

When embedding the dashboard in some other place, you can add the URL query
parameter `var-exp_id` to set the experiment ID. (Change the experiment ID in
the dashboard and observe how the URL changes.) This also works when just
embedding an individual panel.

In the Dashboard settings, the variable can also be set as “hidden” in which case
there is no dropdown at the top to change its value. This might be useful in the
future.
