# Remark Toolbox  [readme-draft]

The Remark Toolbox will be a tool to analyze the electricity market in
different locations in Germany. It comes with a collection of predefined
smart-grid scenarios and configurable parameters for scenario creation, so you
can create the use case you want to simulate. The focus of the Remark Toolbox
is market design for auxilliary services, in particular reactive power.

The core element of the simulation is the smart-grid co-simulation framework
[mosaik](https://gitlab.com/mosaik) developed by OFFIS e.V. in Oldenburg.
It integrates different simulators into a scenario environment, connects them,
manages data flow and runs the simulation. The MultI-DomAin test Scenario
[MIDAS](https://gitlab.com/midas-mosaik/midas) is an extention of mosaik,
containing a collection of mosaik simulators, supporting the integration of
other simulator and is mainly used here for the semi-automatic scenario
configuration. Mango-Mosaik is an implementation of a multi-agent system
(mango) in mosaik, which is based on the Mango Mosaik Electricity Market
[Mamoem](https://gitlab.com/digitalized-energy-systems/models/mamoem).


## Running using Docker

Before you can run the docker containers using ```docker compose``` you need to
rename the *.env.example* and *.txt.example* files by removing *.example*,
specify your own Username and Passwords in the files:

- Define Grafana Username and Passwords in file:
    * ./grafana/grafana_secrets.env
- Define InfluxDB Admin token in file:
    * ./influx/db_admin_token.txt
- Define InfluxDB Username and Password in file:
    * ./influx/influx_secrets.env

Then, you need to build the Remark Core container using
```bash
$ docker build -t remark_core core
```

Finally, run the simulation using docker-compose, simply run
```bash
$ docker compose up --build
```

(Tip: Run
```
$ clear && docker build -t remark_core . && docker compose up --build
```
to get everything up and running at once.)

This Orchestrator container allows you to start simulation runs using REST
requests.

From a second terminal, send your desired scenario to the Orchestrator, for
example using
```bash
$ curl -X POST http://localhost:5000/scenarios -T midas_scenarios.yml
```

Take note of the returned `scenario_id` (including the `scenario_` prefix) and
start a simulation run using
```bash
$ curl -X POST http://localhost:5000/runs/$scenario_id
```
replacing `$scenario_id` by the scenario ID from the previous step.

This should return both the `run_id` and the URL of our Grafana dashboard.
(Ctrl + clicking should open it in a browser window on most operating systems.)

## Running without docker

If you want to run the simulation outside of Docker, for example while
developing, you can install it like this:

0.  Install Poetry using the official instructions here:
    [Installing Poetry](https://python-poetry.org/docs/master/#installing-with-the-official-installer)
    if you haven’t done so already.

1.  Go to the `core` folder:

    ```bash
    $ cd core
    ```

2.  Install our dependencies using

    ```bash
    $ poetry install
    ```

2.  For using simulators from midas it is necessary to configure them and
    download their data sets. To do this, run

    ```bash
    $ poetry run midasctl configure
    ```

    We recommend that you save the midas-runtime-conf.yml in the current
    directory but that you leave the MIDAS data in the standard location.
    (Unfortunately, MIDAS will recommend putting the data in the current
    directory as well if you decide to put the midas-runtime-conf.yml there. So
    you need to copy the path recommended for the midas-runtime-conf.yml when
    asked about the data path.) The midas_scenarios folder needs to be in the
    current directory. As we are using Influx instead of HDF5, the location of
    the _output directory does not matter for us.

    (You can check the configured paths at the end of the
    midas-runtime-conf.yml.)

3.  Under the key `modules.default_modules` change the line beginning with
    `- [store, …]` to

    ```yaml
        - [store, midas.modules.influx.module:InfluxModule]
    ```

4.  Download the MIDAS data using

    ```bash
    $ poetry run midasctl download
    ```

    If you have used MIDAS previously and stored the data in the same location,
    the data will not be downloaded again. (Hence the recommendation to keep
    the data in the standard location in the previous step.)

5.  Install InfluxDB with organization and bucket both named `remark` and create
    an API token.

6.  Set up the environment variables for the Influx connection:

    | Variable | Value |
    | - | - |
    | `INFLUX_TOKEN` | the API token you generated in the previous step |
    | `INFLUX_URL` | the URL (and port) to your Influx instance; the default value of `http://localhost:8086` will often simply work |

    (You can set an environment variable using `$ export VARNAME=value`.)
    You will need to repeat this each time you open a new terminal window.

7.  If you did not configure MIDAS to use the midas_scenarios folder, copy
    test_scenario.yml from there to your MIDAS scenarios folder.

8.  Return to the main folder

    ```bash
    $ cd ..
    ```

8.  Run the simulation with

    ```bash
    $ ./run.sh remark
    ```
    where you can replace `remark` by the name of any MIDAS scenario in the
    midas_scenarios folder. This will first create a corresponding `run` file in
    the midas_scenarios folder and then run the scenario. The script also prints
    out the ID of the run both before and after running MIDAS. Currently, there
    is no automatic Grafana setup if you run the simulation like this.
