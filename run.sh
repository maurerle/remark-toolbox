#!/bin/zsh

pushd core
RUN_ID=$(poetry run python ../orchestrator/src/run_preparation.py $1 ../midas_scenarios http://localhost:8086)
echo "Starting $RUN_ID"
poetry run midasctl run $RUN_ID
echo "Completed $RUN_ID"
popd
