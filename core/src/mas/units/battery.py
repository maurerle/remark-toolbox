from __future__ import annotations

from math import nan
from loguru import logger
from mango.container.core import Container

from mas.agent_messages import (
    BatteryInputMessage,
    FlexibilityMessage,
    ObligationMessage,
    OutputMessage,
    RegisterAtMPMessage,
    RequestFlexibilityMessage,
    RequestOutputMessage,
    SetupDoneMessage,
)
from mas.mangomosaik_agent import MaMoAgent
from mas.types import AgentAddress

from util.flexibility import Flexibility
from util.time_storage import TimeStorage

logger = logger.bind(context="mas")


class BatteryAgent(MaMoAgent):
    mp: AgentAddress
    current_soc: float
    current_time: int
    obligations_kw: TimeStorage[float]
    capacity_kwh: float
    charge_efficiency: float
    discharge_efficiency: float
    min_p_kw: float
    max_p_kw: float

    def __init__(
        self,
        aid: str,
        container: Container,
        mp: AgentAddress,
        observer: AgentAddress,
    ):
        super().__init__(aid, container, observer)
        self.mp = mp
        # TODO: Get these values from the simulation’s configuration
        self.capacity_kwh = 0.5
        self.min_p_kw = -0.25
        self.max_p_kw = 0.25
        self.charge_efficiency = 0.97
        self.discharge_efficiency = 0.97
        self.obligations_kw = TimeStorage(
            lambda _: 0.0, interval_size=900, window_size=8
        )
        self.current_soc = nan

    def handle_parsed_msg(self, message, sender, conversation_id):
        if isinstance(message, SetupDoneMessage):
            self.send_message(
                RegisterAtMPMessage(),
                to=self.mp,
            )

        if isinstance(message, BatteryInputMessage):
            self.current_time = message.time
            self.current_soc = message.soc_percent / 100

        if isinstance(message, RequestFlexibilityMessage):
            charge = self.projected_charge(message.time)
            self.send_message(
                FlexibilityMessage(
                    Flexibility(
                        max_p=min(charge / 0.25, self.max_p_kw),
                        min_p=max((charge - self.capacity_kwh) / 0.25, self.min_p_kw),
                    )
                ),
                to=self.mp,
            )

        if isinstance(message, RequestOutputMessage):
            self.send_message(
                OutputMessage(
                    output={
                        "p_set_mw": -self.obligations_kw[self.current_time] / 1000,
                    },
                ),
                to=sender,
                conversation_id=conversation_id,
            )

        if isinstance(message, ObligationMessage):
            for time in range(
                message.supply_start,
                message.supply_start + message.supply_duration,
                900,
            ):
                self.obligations_kw[time] = message.supply_amount_kw

    def projected_charge(self, time):
        charge = self.capacity_kwh * self.current_soc
        for t in range(self.current_time, time):
            if obligation_kw := self.obligations_kw[t] > 0:  # Discharge
                charge -= 0.25 * obligation_kw / self.discharge_efficiency  # 0.25 hours
            else:
                charge += 0.25 * obligation_kw * self.charge_efficiency
        return charge
