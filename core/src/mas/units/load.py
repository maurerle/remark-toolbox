from __future__ import annotations

from mango.container.core import Container

from mas.agent_messages import (
    FlexibilityMessage,
    LoadInputMessage,
    RegisterAtMPMessage,
    RequestFlexibilityMessage,
    SetupDoneMessage,
)
from mas.mangomosaik_agent import MaMoAgent
from mas.types import AgentAddress
from util.flexibility import Flexibility
from util.time_storage import TimeStorage


class LoadAgent(MaMoAgent):
    def __init__(
        self,
        aid: str,
        container: Container,
        mp: AgentAddress,
        observer: AgentAddress,
    ):
        super().__init__(aid, container, observer)
        self.mp = mp
        self.p_forecast_mw = TimeStorage(
            lambda _: 0.0, interval_size=900, window_size=8
        )

    def handle_parsed_msg(self, message, sender, conversation_id):
        if isinstance(message, SetupDoneMessage):
            self.send_message(
                RegisterAtMPMessage(),
                to=self.mp,
            )

        if isinstance(message, LoadInputMessage):
            self.time = message.time
            for i, p_mw in enumerate(message.p_forecast_mw):
                self.p_forecast_mw[self.time + i * 900] = p_mw

        if isinstance(message, RequestFlexibilityMessage):
            self.send_message(
                FlexibilityMessage(
                    Flexibility(
                        min_p=-self.p_forecast_mw[message.time] * 1000,
                        max_p=-self.p_forecast_mw[message.time] * 1000,
                    )
                ),
                to=self.mp,
            )
