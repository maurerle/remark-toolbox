from __future__ import annotations

from loguru import logger
from typing import Any, Dict, Set, Tuple

from mango import Agent
from mango.container.core import Container
from mango.messages.message import Performatives

from mas.agent_messages import (
    AgentsTerminatedMessage,
    IdleMessage,
)


observer_logger = logger.bind(context='observer')


class ObserverAgent(Agent):
    """
    An ObserverAgent is used for termination detection of the step-method
    """
    _depth_set: Set[int]
    """An element d in this set corresponds to the weight 2^(-d).

    The MosaikAgent starts each communication sequence with a weight of 1 (i.e.
    a depth of 0). Each agent always passes on some of its weight (in the form of a
    depth) with each message it sends and then sends the remaining weight to the
    observer. Once the total weight has reached the observer, the simulation
    terminates.

    This is essentially the technique from https://doi.org/10.1109/ICDCS.1989.37933
    but with window size h=1 (so we only need the window index, which we call depth).
    """

    def __init__(self, container: Container):
        super().__init__(container)
        self._depth_set = set()

    def handle_message(self, content, meta: Dict[str, Any]):
        message = content['message']

        observer_logger.debug(
            f'{self.aid} received message {message} with meta {meta}.'
        )

        match message:
            case IdleMessage():
                self._handle_idle_msg(content['message_depth'])
            case _:
                observer_logger.warning(f"Received unknown message {message}.")

    def add_depth(self, depth):
        """Recursively add the depth to our set of received depths. This is
        maybe easier to understand if you think of a depth d as the number 2^(-d).
        (In this case, the recursive calls are simply the "carry" case of binary
        addition.)
        """
        if depth in self._depth_set:
            self._depth_set.remove(depth)
            self.add_depth(depth - 1)
        else:
            self._depth_set.add(depth)

    def _handle_idle_msg(self, depth: int):
        """
        Receive IdleMessage and trigger termination detection.
        If termination detected, send message to mosaikAgent.
        """
        self.add_depth(depth)
        observer_logger.debug(f'Adding {depth}; depth set now is {self._depth_set}')
        if 0 in self._depth_set:
            message = AgentsTerminatedMessage()
            self.schedule_instant_task(
                self.send_acl_message(
                    receiver_addr=self.mosaik_addr,
                    receiver_id=self.mosaik_id,
                    content={
                        'message': message,
                        'time_at_sender': None,
                    },
                    acl_metadata={
                        'performative': Performatives.inform,
                        'conversation_id': None,
                        'sender_id': self.aid,
                    },
                )
            )
            self._depth_set = set()

    def setup_done(self, mosaik_addr: Tuple[str, int], mosaik_id: str):
        """
        Callback during setup_done of mosaik. Used to initialize address of MosaikAgent.
        """
        self.mosaik_addr = mosaik_addr
        self.mosaik_id = mosaik_id
