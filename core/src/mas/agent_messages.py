import sys
import inspect
from dataclasses import dataclass
import mango.messages.codecs as codecs
from mango.messages.codecs import json_serializable
from typing import Any, Dict, List, Literal, Optional

from mas.market.auction import Bid
from util.flexibility import Flexibility


# Messages of mango mosaik connection
@json_serializable
@dataclass
class SetupDoneMessage:
    pass


@json_serializable
@dataclass
class InputMessage:
    """
    Contains current time in simulation as Unix timestamp
    """
    time: int


@json_serializable
@dataclass
class BatteryInputMessage(InputMessage):
    soc_percent: float


@json_serializable
@dataclass
class LoadInputMessage(InputMessage):
    p_forecast_mw: List[float]


@json_serializable
@dataclass
class AgentsTerminatedMessage:
    """
    Information about detected termination
    """


@json_serializable
@dataclass
class RequestOutputMessage:
    """
    --> 1. RequestInformationMessage: MosaikAgent -> Mo/Mp
    2. AuctioninformationMessage: Mo/Mp -> MosaikAgent
    """
    requested_information: List[str]


@json_serializable
@dataclass
class RegistrationMessage:
    """
    To register market participants at grid operator
    """
    topic: str


@json_serializable
@dataclass
class RegisterAtMPMessage:
    pass


@json_serializable
@dataclass
class RequestFlexibilityMessage:
    time: int


@json_serializable
@dataclass
class FlexibilityMessage:
    flexibility_kw: Flexibility


@json_serializable
@dataclass
class OpeningAuctionMessage:
    """
    communicating (auction_product, supply_start, supply_duration), tender amount and
    gate closure time
    """
    auction_id: str
    product_type: str
    supply_start: int
    supply_duration: int
    tender_amount: float
    gate_opening_time: int
    gate_closure_time: int
    pricing_rule: Literal["pay_as_bid_pricing", "uniform_pricing"]


@json_serializable
@dataclass
class BidMessage:
    """
    Containing bid: (auction_product, supply_start, supply_duration), amount, price
    """
    auction_id: str
    amount: float
    price: float


@json_serializable
@dataclass
class MarketResultMessage:
    """
    Containing bid: (auction_product, supply_start, supply_duration), amount, price
    Todo: replace in code by BidMessage and appropriate usage of ACL performatives.
    """
    auction_id: str
    pricing_rule: str
    amount: int
    marginal_price: float
    scenario: Optional[str] = None


@json_serializable
@dataclass
class AuctionResultsMessage:
    """
    Containing bid: (auction_product, supply_start, supply_duration), amount, price
    Todo: replace in code by BidMessage and appropriate usage of ACL performatives.
    """
    auction_id: str
    winning_bids: List[Bid]
    losing_bids: List[Bid]
    scenario: Optional[str] = None


@json_serializable
@dataclass
class ObligationMessage:
    supply_start: int
    supply_duration: int
    supply_amount_kw: float


@json_serializable
@dataclass
class OutputMessage:
    output: Dict[str, Any]


@json_serializable
@dataclass
class IdleMessage:
    """
    Sent by agents to the Observer to perform it about the unused weight.

    All relevant information is contained in the "message_depth" field that
    is sent with every message (not just this one).
    """
    pass


CLSMEMBERS = [c for _, c in inspect.getmembers(sys.modules[__name__], inspect.isclass)]


codec = codecs.JSON()
for message_type in CLSMEMBERS:
    codec.add_serializer(*message_type.__serializer__())
