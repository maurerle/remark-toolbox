"""
Gateway between mosaik and the MAS.

The class :class:`MosaikAPI` implements the `low-level mosaik API`_.

The MosaikAPI also manages the root main_container for the MAS.  It starts
a :class:`MosaikAgent` and a :class:`ObserverAgent` agent within that main_container.
The MosaikAgent serves as a gateway between the Mo/Mp agents
and mosaik.

The Mo/Mp agents do not run within the root main_container but in a separate
containers in sub processes. These subprocesses are as well managed by the MosaikAPI.

The entry point for the MAS is the function :func:`main()`.  It parses the
command line arguments and starts the :func:`run()` coroutine which runs until
mosaik sends a *stop* message to the MAS.

.. _mosaik API:
   https://mosaik.readthedocs.org/en/latest/mosaik-api/low-level.html

"""
from __future__ import annotations

import asyncio
from datetime import datetime
from loguru import logger
from mango import create_container
from mango.messages.message import Performatives
import mosaik_api
import random
import sys
from typing import Any, Dict, List, Tuple

from mas.agent_messages import (
    AgentsTerminatedMessage,
    BatteryInputMessage,
    InputMessage,
    LoadInputMessage,
    OutputMessage,
    RequestOutputMessage,
    SetupDoneMessage,
    codec,
)
from mas.mangomosaik_agent import MaMoAgent
from mas.market.mo import MoAgent
from mas.market.mp import MpAgent
from mas.observer.observer import ObserverAgent
from mas.units.battery import BatteryAgent
from mas.units.load import LoadAgent
from mas.types import AgentAddress


mosaik_logger = logger.bind(context="mosaik")


def main():
    logger.remove()
    logger.add(sys.stderr, level="WARNING")
    logger.add(
        "logs/mosaik.log", filter=lambda a: a["extra"].get("context") == "mosaik"
    )
    logger.add("logs/mp.log", filter=lambda a: a["extra"].get("context") == "mp")
    logger.add("logs/mo.log", filter=lambda a: a["extra"].get("context") == "mo")
    logger.add(
        "logs/termination.log",
        filter=lambda a: a["extra"].get("context") == "termination",
    )
    logger.add(
        "logs/observer.log", filter=lambda a: a["extra"].get("context") == "observer"
    )
    logger.add(
        "logs/auction.log",
        mode="w",
        filter=lambda a: a["extra"].get("context") == "auction",
    )
    logger.add(
        "logs/mamo_agent.log",
        filter=lambda a: a["extra"].get("context") == "mamo_agent",
    )
    logger.add(
        "logs/messages.log",
        mode="w",
        level="TRACE",
        filter=lambda r: r["extra"].get("context") == "message",
    )

    return mosaik_api.start_simulation(MosaikAPI())


# The simulator meta data that we return in "init()":
META = {
    "api_version": "3.0",
    "type": "time-based",
    "models": {
        "MoAgent": {
            "public": True,
            "params": ["eid"],
            "attrs": [
                "auction_id_tender_amount_supply_start",
                "auction_id_bids",
            ],
        },
        "MpAgent": {
            "public": True,
            "params": ["mo_eid"],
            "attrs": [
                "p_set_kw",  # set power to DER
                "set_percent",  # set percent to DER
                "soc_percent",  # soc of battery from DER
                "p_mw_forecast",  # power from load
            ],
        },
        "BatteryAgent": {
            "public": True,
            "params": ["mp_eid"],
            "attrs": [
                # In
                "soc_percent",
                # Out
                "p_set_mw",
            ],
        },
        "LoadAgent": {
            "public": True,
            "params": ["mp_eid"],
            "attrs": [
                # In
                "p_forecast_mw",
            ],
        },
    },
}


class MosaikAPI(mosaik_api.Simulator):
    """
    Interface to mosaik.
    """

    step_size: int
    bookkeeping_container: 'Container'
    agent_container: 'Container'

    loop: asyncio.AbstractEventLoop
    stopped: asyncio.Future

    mosaik: None
    mosaik_agent: MosaikAgent
    observer_agent: ObserverAgent
    trader_agents: Dict[None, None]
    start_date: datetime
    container_procs: List[None]
    uids: Dict[None, None]
    model_counts: Dict[str, int]

    def __init__(self):
        # We need to pass the META data to the parent class which will extend
        # it and store it as "self.meta":
        super().__init__(META)
        # We have a step size of 15 minutes specified in seconds:
        self.step_size = 60 * 15  # seconds
        self.host = None
        self.port = None

        # This future will be triggered when mosaik calls "stop()":
        self.stopped = asyncio.Future()

        # Set by "run()":
        self.mosaik = None  # Proxy object for mosaik

        self.loop = asyncio.new_event_loop()
        asyncio.set_event_loop(self.loop)

        # Set in "init()"
        self.sid = None  # Mosaik simulator IDs

        self.bookkeeping_container = None  # type: ignore  # set in mosaik init
        """Mango container for all agents that are necessary for the mango-mosaik
        interface."""
        self.agent_container = None  # type: ignore  # set in mosaik init
        """Mango container for agents concerned with the subject matter."""
        self.mosaik_agent = None  # type: ignore  # set in mosaik init
        self.mp_agents = {}
        self.observer_agent = None  # type: ignore  # set in mosaik init
        self.start_date = None  # type: ignore  # set in mosaik init
        self.container_procs = []  # List of "(proc, container_proxy)" tuples

        # Set/updated in "setup_done()"
        self.uids = {}  # agent_id: unit_id

        self.model_counts = {model: 0 for model in META["models"]}

    def init(self, sid, time_resolution=1.0, **sim_params):
        self.loop = asyncio.get_event_loop()
        self.loop.run_until_complete(
            self.init_as_coro(sid, time_resolution, sim_params)
        )
        return META

    async def init_as_coro(self, sid, time_resolution, sim_params):
        """Create a local agent main_container and the mosaik agent."""
        self.sid = sid
        self.host = sim_params["containers"]["bookkeeping"]["host"]
        self.port = sim_params["containers"]["bookkeeping"]["port"]

        self.start_date = datetime.strptime(
            sim_params["start_date"],
            "%Y-%m-%d %H:%M:%S%z",
        )
        self.random_generator = random.Random(sim_params["seed"])

        if not (
            bookkeeping_container := await create_container(
                addr=(
                    sim_params["containers"]["bookkeeping"]["host"],
                    sim_params["containers"]["bookkeeping"]["port"],
                ),
                codec=codec,
            )
        ):
            raise ValueError(
                "Could not start bookkeeping container at address "
                f"{sim_params['containers']['bookkeeping']}"
            )
        self.bookkeeping_container = bookkeeping_container

        if not (
            agent_container := await create_container(
                addr=(
                    sim_params["containers"]["agents"]["host"],
                    sim_params["containers"]["agents"]["port"],
                ),
                codec=codec,
            )
        ):
            raise ValueError(
                "Could not start agent container at address "
                f"{sim_params['containers']['agents']}"
            )
        self.agent_container = agent_container

        # Observer for termination detection
        self.observer_agent = ObserverAgent(self.bookkeeping_container)

        # Start the MosaikAgent agent in the root main_container:
        self.mosaik_agent = MosaikAgent(
            container=self.bookkeeping_container,
            observer=AgentAddress(
                self.bookkeeping_container.addr, self.observer_agent.aid
            ),
        )

    def create(self, num, model, **model_conf):
        entities = self.loop.run_until_complete(
            self.create_as_coro(num, model, **model_conf)
        )
        return entities

    async def create_as_coro(self, num, model, **model_conf):
        """Create *num* instances of *model* and return a list of entity dicts
        to mosaik."""
        assert model in META["models"]
        entities = []

        mosaik_logger.debug("Model config: {}".format(model_conf))
        # Get the number of agents created so far and count from this number
        # when creating new entity IDs:
        container = self.agent_container
        observer = self.observer_agent
        assert observer
        observer_addr = AgentAddress(self.bookkeeping_container.addr, observer.aid)

        mosaik_logger.debug(
            f"Creating {num} instance(s) of {model} using {model_conf=}"
        )
        for _ in range(num):
            eid = model_conf.get("eid", f"{model}_{self.model_counts[model]}")
            self.model_counts[model] += 1
            match model:
                case "MoAgent":
                    agent = MoAgent(
                        aid=eid,
                        container=container,
                        observer=observer_addr,
                    )
                    self.mosaik_agent.market_operators[eid] = AgentAddress(
                        container.addr, agent.aid
                    )
                case "MpAgent":
                    # Entity data
                    mo_id = self.mosaik_agent.agents[model_conf["mo_eid"]][1]
                    agent = MpAgent(
                        aid=eid,
                        container=container,
                        mo=AgentAddress(container.addr, mo_id),
                        observer=observer_addr,
                        seed=self.random_generator.random(),
                    )
                case "BatteryAgent":
                    mp_id = self.mosaik_agent.agents[model_conf["mp_eid"]][1]
                    agent = BatteryAgent(
                        aid=eid,
                        container=container,
                        mp=AgentAddress(container.addr, mp_id),
                        observer=observer_addr,
                    )
                case "LoadAgent":
                    mp_id = self.mosaik_agent.agents[model_conf["mp_eid"]][1]
                    agent = LoadAgent(
                        aid=eid,
                        container=container,
                        mp=AgentAddress(container.addr, mp_id),
                        observer=observer_addr,
                    )
                case _:
                    raise ValueError(f"Model type {model} invalid.")

            entities.append({"eid": eid, "type": model})

            # Store details in agents dict
            self.mosaik_agent.agents[eid] = (
                AgentAddress(container.addr, agent.aid),
                model,
            )
        # print('Entities: ', entities)
        mosaik_logger.debug("Entities: {}".format(entities))
        # print('Mosaik agents: ', self.mosaik_agent.agents)
        mosaik_logger.debug("Mosaik Agents: {}".format(self.mosaik_agent.agents))
        return entities

    def setup_done(self):
        """
        Get the entities that our agents are connected to once the scenario
        setup is done.
        Trigger intialization of observer agent.
        Trigger registration of all
        mo agents at upstream mo and of all and mp agents at connected mo.
        """
        self.observer_agent.setup_done(
            mosaik_addr=self.bookkeeping_container.addr,
            mosaik_id=self.mosaik_agent.aid,
        )

        data = {}
        for eid in self.mosaik_agent.agents:
            data[eid] = None

        self.loop.run_until_complete(self.mosaik_agent.setup_done())

        mosaik_logger.info("Setup done... done")

    def finalize(self):
        """

        :return:
        """
        # TODO
        # await self.controller.shutdown()
        # await self.mosaik_agent.shutdown()
        # await self.main_container.shutdown()
        # await self.agent_container.shutdown()
        pass

    def step(self, time, inputs, max_advance):
        """Send the inputs of the controlled units to our agents and get new
        set-points for these units from the agents.

        This method will run for at most "step_size" seconds, even if the
        agents need longer to do their calculations.  They will then continue
        to do stuff in the background, while this method returns and allows
        mosaik to continue the simulation.

        """
        mosaik_logger.debug(f"{inputs=}")

        self.loop.run_until_complete(self.mosaik_agent.step(time=time, inputs=inputs))

        return time + self.step_size

    def get_data(self, outputs):
        """Return the data requested by mosaik.  *outputs* is a dict like::

            {
                'wecs-0': ['v', 'P'],
                ...
            }

        Return a dict with the requested data::

            {
                'wecs-0': {
                    'v': 23,
                    'P': 42,
                },
                ...
            }
            {
                'mo-0': {
                    'name': 'test‘,
                },
                ...
            }

        """
        data = self.loop.run_until_complete(self.mosaik_agent.gather_outputs(outputs))
        # print("This is the data in get_data: ", data)
        return data


class MosaikAgent(MaMoAgent):
    """This agent is a gateway between the mosaik API and the Mo/Mp agents.

    It forwards the current inputs to the agents, contains Nature for splitting market,
    is in contacts with the termination detection
    and then collects new set-points from the simulated agents.
    """

    agents: Dict[str, Tuple[AgentAddress, str]]
    """Mapping entity ID to agent address and mosaik model type."""
    market_operators: Dict[str, AgentAddress]
    """Mapping entity ID to market operator address"""
    output_data: Dict[str, Dict[str, Any]]
    """Output to mosaik, collected as OutputMessages come in"""

    _agents_terminated: asyncio.Future
    """Future that is triggered whenever the agents are done with their negotiation."""

    def __init__(self, container: 'Container', observer: AgentAddress):
        super().__init__("Mosaik", container, observer)
        self.agents = {}
        self.market_operators = {}

        # Done if the controller has performed one cycle
        self._agents_terminated = asyncio.Future()

        self.output_data = {}

        # We set this to None so future message detection can use it as a sentinel
        # value.
        # TODO: Find a cleaner way to do this.
        self.time = None  # type: ignore

    async def setup_done(self):
        self.reset()
        message_tasks = []
        for mosaik_eid, (agent, _) in self.agents.items():
            message_tasks.append(
                self.send_message(
                    SetupDoneMessage(),
                    to=agent,
                    conversation_id=mosaik_eid,
                )
            )
        await asyncio.gather(*message_tasks)
        self._inform_observer()

    async def step(self, time, inputs):
        """
        This will be called from the mosaik api once per step.

        :param inputs: the input dict from mosaik:
            {eid_1: {'P': p_value}, eid_2: {'P': p_value}...}
        :return: the output dict: {eid_1: p_max_value, eid_2: p_max_value}
        """

        self.reset()

        # Add agents without input data to the inputs dictionary so that they
        # still receive a message containing the new simulation time.
        for eid in self.agents:
            if eid not in inputs:
                inputs[eid] = {}

        await self.update_agents(time, inputs)
        await asyncio.wait_for(self._agents_terminated, timeout=3)
        mosaik_logger.debug("Agents terminated.")

    def handle_message(self, content, meta):
        """
        We expect two types of messages:
        1) AuctionInfirmationMessage with
            a) data from Mo agents about cleared auctions
            b) set points from Mp agents for DER power
        2) Termination notification from observer agent

        :param content: Content of the message
        :param meta: Meta information
        """
        message = content["message"]
        mosaik_logger.debug(f"Mosaik agent received {message} with meta {meta}")

        match message:
            case OutputMessage(output):
                self.message_depth = content["message_depth"]
                agent_eid = next(
                    eid
                    for eid, (agent, _) in self.agents.items()
                    if agent.agent_id == meta["sender_id"]
                )
                self.output_data[agent_eid] = output
                self._inform_observer()

            case AgentsTerminatedMessage():
                # set future to true in order to end mosaik step.
                # print('Result of agents_terminated: ',self._agents_terminated._result)
                self._agents_terminated.set_result(True)

            case _:
                mosaik_logger.warning(f"Received unknown message {message}.")

    async def update_agents(self, time: int, data):
        """Update the agents with new data from mosaik."""
        futs = []
        for mosaik_eid, agent_input in data.items():
            agent, model = self.agents[mosaik_eid]
            match model:
                case "BatteryAgent":
                    msg = BatteryInputMessage(
                        time=time,
                        soc_percent=next(
                            iter(agent_input["soc_percent"].values())
                        ),  # only first
                    )
                case "LoadAgent":
                    p_forecast_mw = next(iter(agent_input["p_forecast_mw"].values()))
                    if not isinstance(p_forecast_mw, list):
                        p_forecast_mw = [p_forecast_mw] * 8
                    msg = LoadInputMessage(
                        time=time,
                        p_forecast_mw=p_forecast_mw,
                    )
                case _:
                    msg = InputMessage(
                        time=time,
                    )

            fut = self.send_message(
                msg,
                to=agent,
                performative=Performatives.inform,
                conversation_id=mosaik_eid,
            )
            futs.append(fut)
        await asyncio.gather(*futs)

        self._inform_observer()

    async def gather_outputs(self, outputs):
        """
        According to agent type (found via the data related to the receiver_id),
        data from the auction_information_message (found in parameter content)
        is saved in two attributes of the Mosaik Agent (mp/mo_auction_information).

        :param outputs: dict: contains all agents and the params defined in the
            connect_many_to_one command in scenario.py
        """
        self.reset()
        self.output_data = {}

        for eid in self.agents:
            if eid not in outputs:
                continue

            agent, _ = self.agents[eid]
            self.send_message(
                RequestOutputMessage(requested_information=outputs[eid]),
                to=agent,
                performative=Performatives.request,
            )
        self._inform_observer()
        await asyncio.wait_for(self._agents_terminated, timeout=3)

        mosaik_logger.debug(f"This is the data to return: {self.output_data}")
        return self.output_data

    def reset(self):
        self._agents_terminated = asyncio.Future()
        self.message_depth = 0


if __name__ == "__main__":
    main()
