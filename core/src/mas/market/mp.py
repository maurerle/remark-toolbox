from __future__ import annotations
from collections import defaultdict

import random
from typing import List, Tuple, Dict, Optional
from loguru import logger

from mango.container.core import Container
from mango.messages.message import Performatives

from mas.agent_messages import (
    AuctionResultsMessage,
    BidMessage,
    FlexibilityMessage,
    InputMessage,
    ObligationMessage,
    OpeningAuctionMessage,
    RegisterAtMPMessage,
    RegistrationMessage,
    RequestFlexibilityMessage,
    RequestOutputMessage,
    SetupDoneMessage,
)
from mas.market.auction import Auction
from mas.mangomosaik_agent import MaMoAgent
from mas.types import AgentAddress
from util.flexibility import Flexibility, combine, split_amount
from util.time_storage import TimeStorage


mosaik_logger = logger.bind(context="mp")


class MpAgent(MaMoAgent):
    """
    A GoMpAgent is the “brain” of the simplified communication between
    Grid Operator and Market Participant.
    """

    unit_agents: List[AgentAddress]
    flexibilities_kw: Dict[str, Dict[AgentAddress, Flexibility]]
    current_flexibility_auction_id: Optional[str]
    """
    The ID of the the auction for which we're currently collecting flexibilities.
    """

    auctions: TimeStorage[Optional[Tuple[str, Auction]]]
    """Auctions ordered by gate closure time"""
    # TODO: Maybe TimeStorage is not ideal here?
    open_auctions: Dict[str, Auction]

    def __init__(
        self,
        container: Container,
        aid: str,
        mo: AgentAddress,
        observer: AgentAddress,
        seed,
    ):
        super().__init__(
            aid=aid,
            container=container,
            observer=observer,
        )
        self.mo = mo

        self.random_generator = random.Random(seed)

        self.open_auctions = {}

        self.unit_agents = []
        self.flexibilities_kw = defaultdict(dict)
        self.current_flexibility_auction_id = None

        self.auctions = TimeStorage(lambda _: None, interval_size=900, window_size=6)

    def handle_parsed_msg(self, message, sender, conversation_id):
        """Perform handling of messages, which means
        activate specific message handling.
        :param content: The message content
        :param meta: All meta information in a dict.
        """

        match message:
            case SetupDoneMessage():
                mosaik_logger.debug(
                    f"Agent {self.aid}: "
                    f"Register at mo: {self.mo}"
                )
                self.send_message(
                    RegistrationMessage(topic="balancing_reserve"),
                    to=self.mo,
                    conversation_id="test",
                )

            case RegisterAtMPMessage():
                self.unit_agents.append(sender)

            case InputMessage(time):
                # TODO: reset der variablen vornehmen
                self.time = time

                # generate bid
                if (auction := self.auctions[self.time]) is not None:
                    self.current_flexibility_auction_id = auction[0]
                    for agent_addr in self.unit_agents:
                        self.send_message(
                            RequestFlexibilityMessage(self.time),
                            to=agent_addr,
                            performative=Performatives.request,
                        )

            case FlexibilityMessage(flexibility_kw):
                assert self.current_flexibility_auction_id
                self.flexibilities_kw[self.current_flexibility_auction_id][
                    sender
                ] = flexibility_kw
                if len(
                    self.flexibilities_kw[self.current_flexibility_auction_id]
                ) == len(self.unit_agents):
                    self.send_bid(self.current_flexibility_auction_id)
                    self.current_flexibility_auction_id = None

            case OpeningAuctionMessage():
                self.handle_opening_auction_msg(message)

            case AuctionResultsMessage():
                self.handle_auction_results_msg(message)

            case RequestOutputMessage():
                self.handle_request_output_msg(message, sender, conversation_id)

    def handle_opening_auction_msg(self, message: OpeningAuctionMessage):
        """
        Handles the most recent auction.
        :param content: The message content
        :param meta: All meta information in a dict. -- currently unused

        """
        auction_id = message.auction_id
        # store open auction locally
        auction = Auction(
            product_type=message.product_type,
            supply_start=message.supply_start,
            supply_duration=message.supply_duration,
            tender_amount=message.tender_amount,
            gate_opening_time=message.gate_opening_time,
            gate_closure_time=message.gate_closure_time,
            pricing_rule=message.pricing_rule,
        )
        # Save the auction in auctions storage at our bidding time (which
        # is 15 minutes, i.e. one step, before the gate closure).
        self.auctions[auction.gate_closure_time - 900] = (auction_id, auction)
        self.open_auctions[auction_id] = auction

    def send_bid(self, auction_id):
        """
        Sends a random bid, currently with fixed amount but random price
        :param current_time: The current time in the simulation
        """

        total_flex_kw = combine(self.flexibilities_kw[auction_id].values())

        # If the total flex's max is negative, we need at least as much power,
        # so we make a high bid for that.
        high_bid = max(0, -total_flex_kw.max_p)

        # Additionally, we can use extra power to charge our battery (for example).
        # In total, we can use -total_flex_kw.min_p, but we don't want to buy the
        # high bid again.
        low_bid = max(0, -total_flex_kw.min_p - high_bid)

        self.send_message(
            BidMessage(
                auction_id=auction_id,
                amount=high_bid,
                price=1000,
            ),
            to=self.mo,
        )

        self.send_message(
            BidMessage(
                auction_id=auction_id,
                amount=low_bid,
                price=10,
            ),
            to=self.mo,
        )

    def handle_auction_results_msg(self, message: AuctionResultsMessage):
        """
        Inform via print about won auction and amount
        :param content: The message content
        :param meta: Meta information
        """
        auction = self.open_auctions[message.auction_id]
        amount = sum(bid.amount for bid in message.winning_bids)
        # We need a minus sign at message amount because the unit agents report in the
        # generator convention (i.e. generation is positive) whereas the auction uses
        # consumer convention (i.e. loads are positive.)
        # TODO: Maybe change this or find a better way of doing this?
        try:
            amounts = split_amount(self.flexibilities_kw[message.auction_id], -amount)
            for unit_agent in self.unit_agents:
                self.send_message(
                    ObligationMessage(
                        supply_start=auction.supply_start,
                        supply_duration=auction.supply_duration,
                        supply_amount_kw=amounts[unit_agent],
                    ),
                    to=unit_agent,
                )
        except ValueError:
            logger.exception("Trying to split the won amount failed.")

    def handle_request_output_msg(
        self, message: RequestOutputMessage, sender, conversation_id
    ):
        # TODO: Currently, there is no information for the market participant
        # to return, so no output request should be made
        raise NotImplementedError
