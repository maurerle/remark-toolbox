from __future__ import annotations

from dataclasses import dataclass, field
from typing import Iterable, List, Literal
from loguru import logger
from mango.messages.codecs import json_serializable

from mas.types import AgentAddress


logger = logger.bind(context='auction')


@json_serializable
@dataclass
class Bid:
    __slots__ = ["bidder", "amount", "price"]

    bidder_addr: AgentAddress
    # TODO: Think about the right types here: should these really be floats
    # or should price and/or amount be indivisible at some point (for example,
    # no sub-MW amounts, not sub-ct prices)?
    amount: float
    price: float


@dataclass
class Auction:
    product_type: str
    supply_start: int
    supply_duration: int
    tender_amount: float
    gate_opening_time: int
    gate_closure_time: int
    pricing_rule: Literal["pay_as_bid_pricing", "uniform_pricing"]

    bids: List[Bid] = field(default_factory=list)

    def add_bid(self, bidder_addr: AgentAddress, amount: float, price: float):
        bid = Bid(bidder_addr, amount, price)
        self.bids.append(bid)

    def clear(self, highest_price_wins: bool = True) -> AuctionResult:
        """
        Determine the winning and losing bids of the auction.

        Bids are considered divisible (i.e. a bid with an amount x might be split
        into two bids with amounts w and l (such that x = w + l) and the bid for w
        wins whereas the bid for l loses).

        The price of winning bids is adjusted according to the auction's pricing
        rule, i.e. for uniform pricing it is set the the marginal price.

        :param highest_price_wins: If True, this is cleared as a normal auction (i.e.
        the highest price wins), if False, this is cleared as a reverse auction (i.e.
        the lowest price wins, so the bidders are selling something.)
        TODO: Turn this into a parameter of the auction, probably.

        :returns: An `AuctionResult` containing the winning and losing bids as well
        as the marginal price.
        """
        sorted_bids = sorted(self.bids, key=lambda bid: bid.price)
        if highest_price_wins:
            sorted_bids = reversed(sorted_bids)

        winning_bids = []
        losing_bids = []
        awarded_amount = 0
        for bid in sorted_bids:
            # NOTE: We consider every bid to be divisible, i.e. we can decide to
            # take only parts of the amount.
            if self.tender_amount - awarded_amount > bid.amount:
                # fully award bid
                awarded_amount += bid.amount
                winning_bids.append(bid)
            elif self.tender_amount > awarded_amount:
                # partially award bid
                # TODO: Fairly split among all equally priced bids in this case.
                winning_part = Bid(
                    bid.bidder_addr,
                    self.tender_amount - awarded_amount,
                    bid.price
                )
                losing_part = Bid(
                    bid.bidder_addr,
                    bid.amount - (self.tender_amount - awarded_amount),
                    bid.price
                )
                winning_bids.append(winning_part)
                losing_bids.append(losing_part)
                awarded_amount = self.tender_amount
            elif self.tender_amount == awarded_amount:
                # reject bid
                losing_bids.append(bid)
            else:
                raise ValueError(
                    f"Somehow, {self.tender_amount=} and {awarded_amount=} missed each "
                    "other. (The awarded amount should never exceed the tender amount.)"
                )
        logger.debug(f'Winning: {winning_bids}')
        logger.debug(f'Losing: {losing_bids}')

        if winning_bids:
            marginal_price = winning_bids[-1].price
        elif highest_price_wins:
            marginal_price = float("-Infinity")
        else:
            marginal_price = float("Infinity")

        if self.pricing_rule == "uniform_pricing":
            for bid in winning_bids:
                bid.price = marginal_price

        return AuctionResult(winning_bids, losing_bids, marginal_price)


@dataclass
class AuctionResult:
    winning_bids: Iterable[Bid]
    """
    The winning bids of an auction. The amounts and prices might be adjusted from the
    original bids. (Amounts: when a bid needed to be split. Prices: in case of uniform
    pricing.)
    """
    losing_bids: Iterable[Bid]
    marginal_price: float
