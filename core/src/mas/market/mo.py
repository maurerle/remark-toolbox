from collections import defaultdict
from loguru import logger
from typing import Tuple, Dict
from mango.container.core import Container

from mas.mangomosaik_agent import MaMoAgent
from mas.agent_messages import (
    AuctionResultsMessage,
    BidMessage,
    InputMessage,
    OpeningAuctionMessage,
    OutputMessage,
    RegistrationMessage,
    RequestOutputMessage,
    SetupDoneMessage,
)
from mas.market.auction import Auction, AuctionResult
from mas.types import AgentAddress


logger = logger.bind(context="mo")


class MoAgent(MaMoAgent):
    """
    A MoAgent is the Market Operator and organizes the auctions.
    """

    mp_agents: dict[AgentAddress, list[str]]
    open_auctions: dict[str, Auction]
    cleared_auctions: dict[str, tuple[Auction, AuctionResult]]

    def __init__(
        self,
        aid: str,
        container: Container,
        observer: AgentAddress,
    ):
        super().__init__(
            aid=aid,
            container=container,
            observer=observer,
        )
        self.mp_agents = {}
        self.open_auctions = {}
        self.cleared_auctions = {}

    def handle_parsed_msg(self, message, sender, conversation_id):
        match message:
            case SetupDoneMessage():
                pass

            case InputMessage(time):
                self.time = time
                self._perform_regular_tasks()

            case RegistrationMessage():
                self._handle_registration_msg(message, sender)

            case BidMessage():
                self._handle_bid_msg(message, sender)

            case RequestOutputMessage():
                self.handle_request_information_msg(message, sender, conversation_id)

            case _:
                logger.warning(f"{self.aid} received an unsupported message: {message}")

    def _perform_regular_tasks(self):
        """Perform tasks, which are needed or checked every time step."""

        # TODO: Perform accounting

        # Temporary list because _clear_auction changes open_auctions
        clearable_auctions = []
        for auction_id in self.open_auctions:
            if self.time >= self.open_auctions[auction_id].gate_closure_time:
                clearable_auctions.append(auction_id)

        for auction_id in clearable_auctions:
            self._clear_auction(auction_id)

        # start auction every quarter past X which runs for half an hour
        if self.time % 3600 == 900:
            auction_id = self._open_balancing_reserve_auction(
                current_time=self.time,
                tender_amount=2,
                gate_opening_time=self.time,
                gate_closure_time=self.time + 1800,
            )
            self._inform_connected_agents_about_gate_opening(auction_id)

    def _open_balancing_reserve_auction(
        self,
        current_time: int,
        tender_amount: int,
        gate_opening_time: int,
        gate_closure_time: int,
    ):
        """Is called after auction has been scheduled. According to current
        time defines an auction with tender amount,
        gate opening time und gate closure time as parameters.
        :param current_time: The current time in the simulation
        :param tender_amount: The quantity tendered in the auction
        :param gate_opening_time: Trading is open from this time on for this auction
        :param gate_closure_time: Trading is closed on this time for this auction
        """

        # some values for functional supply start and duration
        supply_start = current_time + 2700
        auction_id = f"Bal_Reserve_for_{supply_start}"
        supply_duration = 3600
        pricing_rule = "uniform_pricing"
        product_type = "balacing_reserve"
        self.open_auctions[auction_id] = Auction(
            product_type=product_type,
            supply_start=supply_start,
            supply_duration=supply_duration,
            tender_amount=tender_amount,
            gate_opening_time=gate_opening_time,
            gate_closure_time=gate_closure_time,
            pricing_rule=pricing_rule,
        )
        logger.debug(
            f"Agent {self.aid}: "
            f"Created auction {auction_id}: {self.open_auctions[auction_id]}"
        )
        return auction_id

    def _inform_connected_agents_about_gate_opening(self, auction_id):
        """Method of upstream and downstream mo to distribute opened GateOpeningMessage
        as auction_data to connected agents (mos and mps).
        :param auction_identifier: Key of open auction
        :param auction_data: contains further parameters of the auction
        """
        auction = self.open_auctions[auction_id]
        message = OpeningAuctionMessage(
            auction_id=auction_id,
            product_type=auction.product_type,
            supply_start=auction.supply_start,
            supply_duration=auction.supply_duration,
            tender_amount=auction.tender_amount,
            gate_opening_time=auction.gate_opening_time,
            gate_closure_time=auction.gate_closure_time,
            pricing_rule=auction.pricing_rule,
        )

        # Inform connected market participants
        for agent in self.mp_agents:
            self.send_message(message, agent)

    def _begin_clear_auctions(self, clearable_auctions: list):
        """Looping through all clearable auctions and call _clear auction.
        :param current_time: The current time in the simulation
        """
        for auction_id in clearable_auctions:
            self._clear_auction(auction_id)

        logger.debug(
            f"Agent {self.aid}: "
            f"Open Auctions: {self.open_auctions}, Current Time: {self.time} "
        )

    def _clear_auction(self, auction_id):
        """Loop through received bids, sort and award amounts and inform winners and
        losers. Uniform-pricing and pay-as-bid pricing implemented.

        :param auction_id: The to be cleared auction
        """
        auction = self.open_auctions[auction_id]

        logger.debug(
            f"Agent {self.aid}: "
            f"END AUCTION: ID: {auction_id}, TA {auction.tender_amount}, "
            f"GateOpen {auction.gate_opening_time}, "
            f"GateClose {auction.gate_closure_time}"
        )
        # perform auction clearing
        auction_result = auction.clear()
        self.cleared_auctions[auction_id] = (auction, auction_result)
        del self.open_auctions[auction_id]

        # Collate bids from the same bidder and inform them
        bidders = defaultdict(lambda: AuctionResultsMessage(auction_id, [], []))
        for bid in auction_result.winning_bids:
            bidders[bid.bidder].winning_bids.append(bid)
        for bid in auction_result.losing_bids:
            bidders[bid.bidder].losing_bids.append(bid)

        for bidder, msg in bidders.items():
            self.send_message(msg, to=bidder)

    def handle_request_information_msg(self, message, sender, conversation_id):
        """To send information to mosaik database."""
        # get information from the meta dict
        # list_addr = meta.get('sender_addr', None)
        # aid = meta.get('sender_id', None)
        # conversation_id = meta.get('conversation_id', None)

        self.send_message(
            OutputMessage(output={}), to=sender, conversation_id=conversation_id
        )

    def _handle_bid_msg(self, message, sender):
        """This function handles the incoming bids and checks if the content is valid
        (deadline not passed, right format).
        It either sends a deny for invalid messages or stores them into received_bids.

        :param content: The content of the SELL message
        :type content: :class:`fmm.protomsg.fmm_message_pb2.FMM_Message`
        :param meta: All meta information in a dict.
        """
        auction_id = message.auction_id
        if auction_id in self.open_auctions and self._deadline_valid(auction_id):
            # store bid in received bids in self.auction[id].bids
            self.open_auctions[auction_id].add_bid(
                bidder_addr=sender, amount=message.amount, price=message.price
            )
        else:
            # TODO: Rückantwort o.ä. schicken
            raise ValueError("Bid received too late")

    def _deadline_valid(self, auction_id):
        """Checking whether the deadline of an auction has not yet been reached.
        :param content: The message content
        """
        # TODO: integrate into Auction.add_bid() together with additional functionality
        # like check for right price range etc.
        gate_closure_time = self.open_auctions[auction_id].gate_closure_time
        return self.time < gate_closure_time

    def _handle_registration_msg(self, message, sender):
        """Stores the agent id and topic attributes of MPAgents in order to send
        them gate openings.

        :param content: Message content of registration message
        :param meta: All meta information in a dict.
        """
        if message.topic == "balancing_reserve":
            self._register_mpagent(message, sender)

    def _register_mpagent(self, message, sender):
        """Stores the agent id and topic attributes of MPAgents in order to send them
        gate openings.

        :param content: Message content of registration message
        :param meta: All meta information in a dict.
        """
        if sender in self.mp_agents:
            topics = self.mp_agents[sender]
        else:
            topics = []
        topics.append(message.topic)
        self.mp_agents[sender] = topics
