from asyncio import Task
from typing import Optional, Dict, Any
from queue import Queue
from loguru import logger

from mango import Agent
from mango.container.core import Container
from mango.messages.message import Performatives

from mas.agent_messages import IdleMessage
from mas.types import AgentAddress


mosaik_logger = logger.bind(context='mamo_agent')
message_logger = logger.bind(context='message')


class MaMoAgent(Agent):
    """
    A MaMoAgent is the general class (e.g. of market operators and market participants),
    which run in mosaik.

    Implements message counting and communication with an observer to detect termination
    of message exchange between agents for the current (Mosaik) time step.
    """

    observer: AgentAddress
    """The observer for termination detection."""
    message_queue: Queue
    """The queue of early messages."""
    time: int
    """The current Mosaik time, as known to this agent."""
    message_depth: int

    def __init__(
        self,
        aid: str,
        container: Container,
        observer: AgentAddress,
    ):
        super().__init__(container, suggested_aid=aid)
        self.observer = observer
        self.message_queue = Queue()

        # Setting this to -1 reduces the None checks for detectng early messages.
        self.time = -1
        self.message_depth = 0

    def handle_message(self, content, meta: Dict[str, Any]):
        """
        This is Mango's message entry point. MaMoAgents should usually overwrite
        `handle_parsed_msg` instead, unless they're directly concerned with the
        interaction between Mango and Mosaik.
        """
        if self._needs_postponed_handling(content):
            self.message_queue.put((content, meta))
            message_logger.trace(
                "Early message at {receiver} at time {time}.",
                receiver=self.aid,
                time=self.time,
            )
            return

        self._handle_msg_now(content, meta)

        # After handling the new message, previously postponed messages should now be
        # ready to handle.
        for _ in range(self.message_queue.qsize()):
            (postponed_content, postponed_meta) = self.message_queue.get()
            if self._needs_postponed_handling(postponed_content):
                # TODO: Better Exception type here?
                raise Exception(
                    'Message still from the future after receiving new time.'
                )
            self._handle_msg_now(postponed_content, postponed_meta)

        self._inform_observer()

    def _handle_msg_now(self, content, meta):
        """
        Parse and handle a message after it has been determined to be timely.
        """
        message = content['message']
        message_logger.trace(
            "Receive {message} ({depth}) from {sender} at {receiver}",
            message=message,
            sender=meta['sender_id'],
            receiver=self.aid,
            depth=content['message_depth']
        )

        self.message_depth = content['message_depth']

        self.handle_parsed_msg(
            message,
            sender=AgentAddress(meta['sender_addr'], meta['sender_id']),
            conversation_id=meta['conversation_id'],
        )

    def _needs_postponed_handling(self, content):
        """
        Occasionally, we might receive messages from the "future".
        This happens if another agent's message reaches us before we have received
        our input message for a step. In this case, the message's handling needs to
        be postponed until we receive our input."""
        if content['time_at_sender'] is None:
            # This is an update message which should be handled immediately.
            return False
        else:
            return content['time_at_sender'] > self.time

    def handle_parsed_msg(
        self,
        message,
        sender: AgentAddress,
        conversation_id: Optional[str],
    ):
        """
        Handle an incoming message after it has been counted for termination
        detection, checked for timeliness, and parsed into an agent_messages.py
        dataclass.

        This method should be overridden instead of `handle_msg`.
        """
        raise NotImplementedError()

    def send_message(
        self,
        message: Any,
        /,
        to: AgentAddress,
        *,
        performative: Performatives = Performatives.inform,
        conversation_id: Any = None,
        send_entire_weight: bool = False,
    ) -> Task:
        """
        Send a (counted) message to receiver.

        "Counted" means counted for our termination detection, so this method
        should *not* be used to send messages to the observer.

        :param message: The message to be sent. Must be a dataclass.
        :param receiver: The receiver of the message as a (addr, id) pair.
        :param performative: The ACL performative for this message.
        :param converstation_id: The conversation ID for this message.
        :param include_in_count: (default `True`) Whether to include this message
        in the count for termination detection. This needs to be set to `False` for
        some messages that concern the termination detection itself.
        """
        # Setting `depth=1` means that the log message will mention the call site
        # of `send_message` instead of this line.
        if not send_entire_weight:
            self.message_depth += 1
        message_logger.opt(depth=1).trace(
            'Send {message} ({depth}) from {sender} to {receiver}',
            message=message,
            sender=self.aid,
            receiver=to.agent_id,
            depth=self.message_depth,
        )
        future = self.schedule_instant_task(
            self.send_acl_message(
                receiver_addr=to.container_addr,
                receiver_id=to.agent_id,
                content={
                    'message': message,
                    'time_at_sender': self.time,
                    'message_depth': self.message_depth,
                },
                acl_metadata={
                    'performative': performative,
                    'conversation_id': conversation_id,
                    'sender_id': self.aid,
                },
            )
        )
        return future

    def _inform_observer(self):
        """
        Send our remaining weight (in the form of our depth) to the observer
        for termination detection.
        """
        if self.observer is not None:
            self.send_message(
                IdleMessage(),
                to=self.observer,
                send_entire_weight=True,
            )
