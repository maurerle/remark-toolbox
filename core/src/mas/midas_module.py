from __future__ import annotations
from dataclasses import dataclass, field

from loguru import logger
from typing import Any, Dict, List, Optional, Tuple, Union
from midas.util.upgrade_module import UpgradeModule


@dataclass(slots=True)
class ConnectionsConfig:
    """Decription of connections between two simulators."""

    attrs: List[Union[str, Tuple[str, str]]]
    """The attributes to connect. (An individual string like "attr" will be expanded
    to the pair ("attr", "attr") by mosaik.)"""
    time_shifted: bool = False
    """Whether the connection between the simulators is time_shifted."""
    initial_data: Dict[str, Any] = field(default_factory=dict)
    """In case of time-shifted connections, the initial values for that connection."""


@dataclass(slots=True)
class UnitAgentConfig:
    """Description of a single type of unit agent."""

    yaml_key: str
    """The YAML key for this agent in the MIDAS scenario."""
    mosaik_model: str
    """The model name in the mosaik interface of the agent simulation."""
    pg_mapping_src_module: str
    """The module that supplies the unit simulator. (As given in the
    powergrid_mapping.)"""
    pg_mapping_src_type: str
    """The (MIDAS) type of the unit simulator. (As given in the
    powergrid_mapping.)"""
    unit_to_agent_connections: Optional[ConnectionsConfig]
    """The mosaik attributes going from the unit to this agent."""
    agent_to_unit_connections: Optional[ConnectionsConfig]
    """The mosaik attributes going from this agent to its unit."""
    agent_to_db_connections: Optional[ConnectionsConfig]
    """The mosaik attributes going from this agent to the database."""


# This describes the different types of unit agents, their configuration in the
# scenario YAML file and how they’re related to other simulators.
UNIT_AGENTS = {
    "load_agent": UnitAgentConfig(
        yaml_key="load_agents",
        mosaik_model="LoadAgent",
        pg_mapping_src_module="sndata",
        pg_mapping_src_type="household",
        unit_to_agent_connections=ConnectionsConfig([("p_mw", "p_forecast_mw")]),
        agent_to_unit_connections=None,
        agent_to_db_connections=None,
    ),
    "battery_agent": UnitAgentConfig(
        yaml_key="battery_agents",
        mosaik_model="BatteryAgent",
        pg_mapping_src_module="der",
        pg_mapping_src_type="BAT",
        unit_to_agent_connections=ConnectionsConfig(
            ["soc_percent"], time_shifted=True, initial_data={"soc_percent": 50.0}
        ),
        agent_to_unit_connections=ConnectionsConfig(["p_set_mw"]),
        agent_to_db_connections=ConnectionsConfig(["p_set_mw"]),
    ),
}


class RemarkAgentsModule(UpgradeModule):
    """
    This implements a MIDAS UpgradeModule for the Remark Agent System.

    The format of the corresponding `params` section of the scenario description is as
    follows:

    ```yaml
    remark_agents_params:
      remark_scope:
        containers:
          bookkeeping:
            host: localhost
            port: 5558
          agents:
            host: localhost
            port: 5557
        traders:
          - {}
          - {}
        load_agents:
          - unit: [1, 1]
            trader: 0
          - unit: [1, 0]
            trader: 1
        battery_agents:
          - unit: [4, 1]
            trader: 0
    ```

    The section `containers` describes the mango containers. The `bookkeeping`
    container holds all the MosaikAgent (responsible for relaying the information
    from mosaik to the other agents and back) and the ObserverAgent (responsible
    for our termination detection). These agents are created automatically.
    The `agents` container holds all agents that are concerned with the simulation
    proper. A market operator is generated automatically in this container.

    All other agents are created as described in the other sections. First, there
    are trader agents, which don't have any setting for now. Second, there are
    different kinds of unit agents. Each of them is connected to a trader
    (referenced by the trader's index in the trader list) and a unit. The unit is
    a given by a pair. The first component of the pair gives the bus id where
    the unit can be found. The second component of the pair gives the index of the
    unit in the list of units given for that bus by some other module. Namely,
    for

    load_agents
    : the index in the list of households for that bus given by the sndata module

    battery_agents
    : the index in the list of batteries for that bus given by the der module's
    PysimmodsBAT simulator
    """

    def __init__(self):
        class LoggingShim:
            def debug(self, *args, **kwargs):
                msg, *format_args = args
                logger.debug(msg % tuple(format_args), **kwargs)
        super().__init__(
            module_name="remark_agents",
            default_scope_name="midasmv",
            default_sim_config_name="RemarkAgents",
            default_import_str="mas.mosaik:MosaikAPI",
            default_cmd_str="%(python)s -m mas.mosaik %(addr)s",
            log=LoggingShim(),  # type: ignore  # loguru loggers don't have the right type
        )

    def check_module_params(self, module_params: Dict[str, Any]):
        pass

    def check_sim_params(self, module_params: Dict[str, Any]):
        self.sim_params.setdefault("start_date", self.params["start_date"])
        # TODO: Use MIDAS seeding
        self.sim_params.setdefault("seed", 42)
        pass

    def start_models(self):
        model_key = self.scenario.generate_model_key(self, "market")
        full_id = self.start_model(
            model_key,
            "MoAgent",
            {"eid": "Market"},
        )
        self.traders = []
        for agent_idx, _ in enumerate(self.sim_params["traders"]):
            model_key = self.scenario.generate_model_key(self, "trader", str(agent_idx))
            full_id = self.start_model(model_key, "MpAgent", {"mo_eid": "Market"})
            _, entity_id = full_id.split(".")
            self.traders.append(
                {
                    "model_key": model_key,
                    "entity_id": entity_id,
                }
            )
        for agent_type, agent_config in UNIT_AGENTS.items():
            for agent_idx, agent_params in enumerate(
                self.sim_params[agent_config.yaml_key]
            ):
                model_key = self.scenario.generate_model_key(
                    self, agent_type, str(agent_idx)
                )
                self.start_model(
                    model_key,
                    agent_config.mosaik_model,
                    {"mp_eid": self.traders[agent_params["trader"]]["entity_id"]},
                )

    def connect(self):
        for agent_type, agent_config in UNIT_AGENTS.items():
            for agent_idx, agent_params in enumerate(
                self.sim_params[agent_config.yaml_key]
            ):
                agent_model_key = self.scenario.generate_model_key(
                    self, agent_type, str(agent_idx)
                )
                bus_id, unit_idx = agent_params["unit"]
                pg_mapping = self.scenario.get_powergrid_mappings(self.scope_name)
                unit_model_key = pg_mapping[bus_id][agent_config.pg_mapping_src_module][
                    agent_config.pg_mapping_src_type
                ][unit_idx]
                if conn_config := agent_config.unit_to_agent_connections:
                    self.connect_entities(
                        unit_model_key,
                        agent_model_key,
                        conn_config.attrs,
                        time_shifted=conn_config.time_shifted,
                        initial_data=conn_config.initial_data,
                    )
                if conn_config := agent_config.agent_to_unit_connections:
                    self.connect_entities(
                        agent_model_key,
                        unit_model_key,
                        conn_config.attrs,
                        time_shifted=conn_config.time_shifted,
                        initial_data=conn_config.initial_data,
                    )

    def connect_to_db(self):
        db_key: str = self.scenario.find_first_model("store", "database")[
            0
        ]  # type: ignore

        for agent_type, agent_config in UNIT_AGENTS.items():
            if conn_config := agent_config.agent_to_db_connections:
                for agent_idx, _ in enumerate(self.sim_params[agent_config.yaml_key]):
                    agent_model_key = self.scenario.generate_model_key(
                        self, agent_type, str(agent_idx)
                    )
                    self.connect_entities(
                        agent_model_key,
                        db_key,
                        conn_config.attrs,
                        time_shifted=conn_config.attrs,
                        initial_data=conn_config.initial_data,
                    )
