from typing import NamedTuple, Tuple


class AgentAddress(NamedTuple):
    container_addr: Tuple[str, int]
    agent_id: str
