from typing import Callable, Generic, List, TypeVar


V = TypeVar("V")


class TimeStorage(Generic[V]):
    """
    Representation of a fixed-length "window" of values moving forward in time.

    For example, if we have the sequence of values

        0  10  20  30  40  50  60  70  ...

    and a window-size of 4, we would be able to access the values 0, 10, 20, 30
    at the indices 0, 1, 2, 3 at first. Once the window moves forward, accessing
    index 0 becomes invalid and we can access the values 10, 20, 30, 40 at the
    indices 1, 2, 3, 4 instead, and so on.

    In this implementation, the window automatically moves forward whenever a
    value in the future is accessed (read or written); trying to access values
    in the past raises an exception.

    In addition to the moving window, the stepping size of the indices can be
    chosen. For example, you might have values at times 0, 900, 1800, 2700, ...
    instead of 0, 1, 2, 3, ...
    """

    _store: List[V]
    """The store for the values. This is organized as a ring buffer, i.e. the
    value at index i should be accessed via
        self._store[i % len(self._store)]
    """
    _start: int
    _shift: int
    _interval_size: int
    _default_factory: Callable[[int], V]

    def __init__(
        self,
        default_factory: Callable[[int], V],
        *,
        window_size: int,
        interval_size: int,
        shift: int = 0,
    ) -> None:
        super().__init__()
        self._default_factory = default_factory
        self._store = [
            default_factory(i * interval_size + shift) for i in range(window_size)
        ]
        self._interval_size = interval_size
        self._start = 0
        self._shift = shift

    def _interval(self, time: int) -> int:
        return (time - self._shift) // self._interval_size

    def __contains__(self, time) -> bool:
        return self._start <= self._interval(time) < self._start + len(self._store)

    def __getitem__(self, time: int) -> V:
        interval = self._interval(time)
        if interval < self._start:
            raise KeyError(f"Time {time} lies to far in the past.")
        self.advance_to(interval + 1)
        return self._store[interval % len(self._store)]

    def __setitem__(self, time: int, value: V) -> None:
        if not (time - self._shift) % self._interval_size == 0:
            raise KeyError(
                f"Time {time} does not fall onto interval start when using "
                f"interval size {self._interval_size} and shift {self._shift}."
            )
        interval = self._interval(time)
        if interval < self._start:
            raise KeyError("Cannot set value in the past.")
        else:
            self.advance_to(interval + 1)
            self._store[interval % len(self._store)] = value

    def advance_to(self, new_end: int):
        """
        Move the window so that it ends at `new_end` (exclusive).

        Calls the default factory to produce necessary intermediate values
        but avoids calls for values that would be overwritten anyways.
        """
        for i in range(
            max(new_end - len(self._store), self._start + len(self._store)),
            new_end,
        ):
            self._store[i % len(self._store)] = self._default_factory(
                i * self._interval_size + self._shift
            )
        self._start = max(self._start, new_end - len(self._store))

    def __repr__(self):
        store_contents = (
            self._store[self._start % len(self._store):]
            + self._store[: self._start % len(self._store)]
        )
        return (
            f"<TimeStorage from {self._start * self._interval_size + self._shift}, "
            f"{(self._start + 1) * self._interval_size + self._shift}, ..., is "
            f"{store_contents}>"
        )
