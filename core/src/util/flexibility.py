from __future__ import annotations

from dataclasses import dataclass
from typing import Any, Dict, Iterable
from mango.messages.codecs import json_serializable


@json_serializable
@dataclass
class Flexibility:
    """
    A very simple flexibility model where a flexibility consists of bounds on the power
    only.

    TODO: We might want to switch to a more sophisticated model that allows "looking
    into the future". My expectation here would be that we can still define `combine`
    and `split_amount` and that if a (sequence of?) values is compatible with the result
    of `combine`, `split_amount` never fails. (And `combine` is (close to) the most
    general Flexibility with that property.)
    """

    min_p: float
    max_p: float


def combine(flexibilities: Iterable[Flexibility]) -> Flexibility:
    return Flexibility(
        min_p=sum(flex.min_p for flex in flexibilities),
        max_p=sum(flex.max_p for flex in flexibilities),
    )


def split_amount(
    flexibilities: Dict[Any, Flexibility], total_amount: float, *, eps=1e-6
) -> Dict[Any, float]:
    """
    Split the given amount among the given flexibilities, so that each flexibility is
    satisfied.

    The result will be a dict containing the same keys as `flexibilities` and for each
    kay one value that lies within the flexibility bounds for that key. Also, the sum
    of the values will be `total_amount` (up to an eps-sized error).

    If no such split is possible, a ValueError will be raised.
    """
    amounts = {key: flex.min_p for key, flex in flexibilities.items()}
    total_assigned = sum(amounts.values())
    if total_assigned > total_amount:
        raise ValueError(
            f"The given {total_amount=} cannot be split according to the "
            f"{flexibilities=}. (The total amount is too small.)"
        )

    for key, flex in flexibilities.items():
        if total_assigned + (flex.max_p - flex.min_p) < total_amount:
            amounts[key] = flex.max_p
            total_assigned = sum(amounts.values())
        else:
            amounts[key] = total_amount - (total_assigned - amounts[key])
            return amounts

    if abs(sum(amounts.values()) - total_amount) <= eps:
        return amounts

    raise ValueError(
        f"The given {total_amount=} cannot be split according to the "
        f"{flexibilities=}. (The total amount it too big.)"
    )
