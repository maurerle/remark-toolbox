import datetime as dt
from mosaik_api import Simulator


META = {
    'type': 'time-based',
    'models': {
        'DateTime': {
            'public': True,
            'any_inputs': False,
            'params': ['step_size', 'initial_datetime'],
            'attrs': ['iso8601']
        }
    }
}


class DateTime(Simulator):
    initial_datetime: dt.datetime
    time_resolution: dt.timedelta
    step_size: int

    def __init__(self):
        super().__init__(META)

    def init(self, sid, time_resolution):
        self.time_resolution = dt.timedelta(seconds=time_resolution)
        return self.meta

    def create(self, num, model, step_size, initial_datetime):
        if num != 1:
            raise ValueError("Must create exactly one entity.")
        if model != 'DateTime':
            raise ValueError(f"Unknown model {model}.")

        self.step_size = step_size
        self.initial_datetime = dt.datetime.fromisoformat(initial_datetime)

        return [{'eid': 'DateTime', 'type': 'DateTime'}]

    def step(self, time, inputs, max_advance):
        self.datetime = self.initial_datetime + time * self.time_resolution
        return time + self.step_size

    def get_data(self, outputs):
        return {
            'DateTime': {
                'iso8601': self.datetime.isoformat(),
            },
        }
