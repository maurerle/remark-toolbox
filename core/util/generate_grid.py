import pandapower as pp
'''
Simple 3 bus grid, compare: https://github.com/e2nIEE/pandapower/blob/master/tutorials/minimal_example.ipynb
1 bus with connection to external grid, and two busses  with each one Household and a PV 

'''
# create empty net
net = pp.create_empty_network()

# create buses
bus_exGrid = pp.create_bus(net, vn_kv=20., name="ExtGridBus")
bus_agent1 = pp.create_bus(net, vn_kv=0.4, name="Agent1Bus")
bus_agent2 = pp.create_bus(net, vn_kv=0.4, name="Agent2Bus")

# create bus elements
pp.create_ext_grid(net, bus=0, vm_pu=1.02, name= 'Grid Connection)')
pp.create_load(net, bus=bus_agent1, p_mw=0.0, name='Load')
pp.create_sgen(net, bus=bus_agent1, p_mw=0.0, name='Generator')

pp.create_load(net, bus=bus_agent2, p_mw=0.0, name='Load')
pp.create_sgen(net, bus=bus_agent2, p_mw=0.0, name='Generator')

# create branch elements
pp.create_line(net, from_bus=1, to_bus=2, length_km=0.1, std_type="NAYY 4x50 SE")
pp.create_transformer(net, hv_bus=0, lv_bus=1, std_type="0.4 MVA 20/0.4 kV")

pp.to_json(net, "simplest_grid.json")

# test
pp.runpp(net)
print(net.res_bus)
print(net.res_load)
print(net.res_sgen)
