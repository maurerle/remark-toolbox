import os
from ruamel.yaml import YAML
import sys
import uuid

yaml = YAML()

def prepare_run(
    scenario_id,
    midas_scenarios_path="midas_scenarios",
    influx_url="http://influxdb:8086",
):
    run_id = f"run_{uuid.uuid4()}"
    influx_token = os.environ["INFLUX_TOKEN"]
    with open(f"{midas_scenarios_path}/{run_id}.yml", "w") as f:
        extension = {
            run_id: {
                "parent": scenario_id,
                "custom_modules": [
                    ["remark_agents", "mas.midas_module:RemarkAgentsModule"],
                ],
                "store_params": {
                    "url": influx_url,
                    "step_size": 900,
                    "org": "remark",
                    "bucket": "remark",
                    "measurement": run_id,
                    "token": influx_token,
                },
                "remark_agents_params": {
                    "remark_scope": {
                        "containers": {
                            "bookkeeping": {
                                "host": "127.0.0.1",
                                "port": 5558,
                            },
                            "agents": {
                                "host": "127.0.0.1",
                                "port": 5557,
                            },
                        },
                    },
                },
            },
        }
        yaml.dump(extension, f)
    return run_id


if __name__ == "__main__":
    print(prepare_run(*sys.argv[1:]))