from typing import Optional
from flask import Flask, Response, request

import docker
from docker.models.containers import Container
import hashlib
from io import StringIO
import os
from ruamel.yaml import YAML
import uuid
import threading
from flask_cors import CORS, cross_origin


from run_preparation import prepare_run


app = Flask(__name__)

"""Enabling CORS for the all paths via flask_cors"""
CORS(app)

yaml = YAML()
docker_client = docker.from_env()


def scenario_list():
    return [
        filename[:-4]
        for filename in os.listdir("midas_scenarios")
        if filename.startswith("scenario")
    ]


def yaml_response(value):
    """Create an HTTP response based the YAML representation of `value`."""
    stream = StringIO()
    yaml.dump(value, stream)
    return Response(stream.getvalue(), mimetype="text/yaml")


@app.get("/scenarios")
def get_scenarios():
    """Return a list of all available scenarios."""
    return yaml_response(scenario_list())


@app.get("/scenarios/<scenario_id>")
def get_scenario(scenario_id):
    """Return the MIDAS scenario with id `scenario_id`."""
    with open(f"midas_scenarios/{scenario_id}.yml", "r") as f:
        scenario = yaml.load(f.read())
    return yaml_response(
        {
            "remark": scenario[scenario_id]
        }
    )

@app.route('/scenario', methods=['POST'])
def post_scenario():
    """Create a new scenario. The payload of the Post request should be a YAML
    describing a single MIDAS scenario named `remark`. The `scenario_id` of the
    given scenario will be returned."""
    scenario_hasher = hashlib.sha256()
    scenario_hasher.update(request.data)
    scenario_id = f"scenario_{scenario_hasher.hexdigest()}"
    scenario = {scenario_id: yaml.load(request.data)["remark"]}
    with open(f"midas_scenarios/{scenario_id}.yml", "w") as f:
        yaml.dump(scenario, f)


    res = Response("scenario_id : " + str(scenario_id))

    #res.headers.add('Access-Control-Allow-Origin', '*')
    #res.headers.add('Access-Control-Allow-Methods', 'POST')
    #res.headers.add('Access-Control-Allow-Headers', 'Content-Type')
    return res



def container_run(run_id):
    """Performs the scenario run `run_id` and waits for it to finish.

    This function is intended as the entry point for a different thread."""
    container: Optional[Container] = docker_client.containers.run(
        "remark_core",
        run_id,
        network="remark-network",
        volumes=["remark-scenarios:/midas_scenarios:ro"],
        detach=True,
    )  # type: ignore
    app.logger.info(f"Started container {container}")
    if container:
        status = container.wait()
        app.logger.info(f"Completed {run_id} with exit code {status}!")


@app.get("/runs/<scenario_id>")
def get_run(scenario_id):
    """Create a new run of scenario `scenario_id` and start the corresponding
    docker container (in a separate thread)."""
    if not scenario_id in scenario_list():
        raise ValueError(f"Unknown scenario: {scenario_id}")
    run_id = prepare_run(scenario_id)
    threading.Thread(target=container_run, args=(run_id,)).start()
    return yaml_response(
        {
            "run_id": run_id
        }
    )

@app.get("/grafana/<run_id>")
def get_grafanaDashboard(run_id):
    return yaml_response(
        {
            "grafana": f"http://localhost:3000/d/MVpw8Wv4z/remark?refresh=5s&var-exp_id={run_id}"
        }
    )



if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)
